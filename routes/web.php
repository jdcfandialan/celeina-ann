<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::middleware("admin")->group(function(){
	//route for adding a new designer
	Route::get('/add-designer', 'CreatorController@add');
	Route::post('/add-designer', 'CreatorController@addDesigner');

	//route for editing a designer
	Route::get('/edit-designer/{id}', 'CreatorController@edit');
	Route::patch('/edit-designer/{id}','CreatorController@update');	

	//route for deleting a designer
	Route::delete('/delete-creator/{id}', 'CreatorController@destroy');

	//route for adding a dress
	Route::get('/add-dress', 'ItemController@add');
	Route::post('/add-dress', 'ItemController@addDress');

	//route for deleting a dress
	Route::delete('/delete-dress/{id}', 'ItemController@delete');

	//route for making a dress available
	Route::get('/return-dress/{id}', 'ItemController@makeAvailable');

	//route for emplyoees
	Route::get('/employees', 'UserController@index');

	//route for deleting employees
	Route::delete('/deactivate-employee/{id}', 'UserController@delete');

	//route for reactivating employees
	Route::get('/reactivate-employee/{id}', 'UserController@reactivate');

	//route for admin edit
	Route::get('/admin-edit/{id}', 'UserController@adminEdit');
	Route::patch('/admin-edit/{id}', 'UserController@update');
});

Route::middleware("employee")->group(function(){
	//route for edit
	Route::get('/edit-dress/{id}', 'ItemController@edit');
	Route::patch('/edit-dress/{id}', 'ItemController@update');
	//route for editing profile
	Route::get('/edit-profile/{id}', 'UserController@edit');
	Route::patch('/edit-profile/{id}', 'UserController@update');
});

Route::middleware("auth")->group(function(){
	//route for listings
	Route::get('/listings', 'ItemController@index');

	//route for creator list
	Route::get('/designers', 'CreatorController@index');

	//route for search results
	Route::post('/search', 'HomeController@search');

});