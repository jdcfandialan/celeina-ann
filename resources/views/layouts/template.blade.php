<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/sandstone/bootstrap.css">
	<link href="https://fonts.googleapis.com/css?family=Qwigley&display=swap" rel="stylesheet">
	<script src="https://kit.fontawesome.com/f6eaa925b8.js" crossorigin="anonymous"></script>

	<style type="text/css">
		*{
			overflow-x: hidden;
		}
	</style>
</head>
<body>
	<div class="row">
		<div class="col-lg-2 bg-primary vh-100" style="position:fixed;z-index:999">
			<div class="d-flex justify-content-center align-items-center">
				<a href="/" style="font-family: Qwigley;font-size:3rem;text-decoration:none" class="text-white">Celeina Ann</a>
			</div>
			<hr style="border-color:gray; margin-left: 10px">
			<div class="d-flex justify-content-center align-items-center flex-column">
				<img src="{{ asset(Auth::user()->imgPath) }}" class="rounded-circle my-2" style="height:40px;width:40px">
				@if(Auth::user()->role_id == 2)
					<a href="/edit-profile/{{ Auth::user()->id }}" class="text-white text-decoration-none">{{ Auth::user()->firstName }} {{ Auth::user()->lastName }}</a>
				@else
					<a href="/admin-edit/{{ Auth::user()->id }}" class="text-white text-decoration-none">{{ Auth::user()->firstName }} {{ Auth::user()->lastName }}</a>
				@endif
			</div>
			<hr style="border-color:gray; margin-left: 10px">
			<div class="d-flex justify-content-center align-items-center flex-column">
				<form method="POST" action="/search">
					@csrf
					<input type="text" name="search" class="form-control my-1" placeholder="Search Database" style="width:180px">
					<button class="btn btn-info btn-block my-1" type="submit" style="width:180px">Search</button>
				</form>
			</div>
			@if(Auth::user()->role_id == 1)
				<hr style="border-color:gray; margin-left: 10px">
				<h6 class="text-white text-center">Admin Functions</h6>
				<div class="d-flex dropdown-item align-items-center justify-content-between">
					<a href="/add-dress" class="text-secondary">Add to Inventory</a>
				</div>
				<div class="d-flex dropdown-item align-items-center justify-content-between">
					<a href="/add-designer" class="text-secondary">Add a Designer</a>
				</div>
			@endif
			<hr style="border-color:gray; margin-left: 10px">
			<h6 class="text-white text-center">Manage</h6>
			<div class="d-flex dropdown-item align-items-center justify-content-between">
				<a href="/listings" class="text-secondary">Inventory</a>
				<i class="fas fa-boxes text-danger"></i>
			</div>
			<div class="d-flex dropdown-item align-items-center justify-content-between">
				<a href="/designers" class="text-secondary">Designers</a>
				<i class="fas fa-cut text-danger"></i>
			</div>
			@if(Auth::user()->role_id == 1)
			<div class="d-flex dropdown-item align-items-center justify-content-between">
				<a href="/employees" class="text-secondary">Employees</a>
				<i class="fas fa-address-card text-danger"></i>
			</div>
			@endif
			<hr style="border-color:gray; margin-left: 10px">
			<div class="d-flex dropdown-item align-items-center justify-content-between">
	            <a class="text-secondary" href="{{ route('logout') }}"
	               onclick="event.preventDefault();
	                document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
	            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	                @csrf
	            </form>
				<i class="fas fa-sign-out-alt text-danger"></i>
			</div>
		</div>
		<div class="col-lg-9 offset-lg-1">
			@yield('content')
		</div>
	</div>
</body>
</html>