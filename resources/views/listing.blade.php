@extends('layouts.template')
@section('content')
	<div class="col-lg-12">
		<div class="row"style=" margin-left: 200px">
			@foreach($items as $item)
				<div class="col-lg-4 my-5">
					<div class="card">
						<div class="card-head">
							<div class="d-flex justify-content-center align-items-center">
								<img src="{{ $item->imgPath }}" alt="broken img" style="object-fit: contain;height: 200px;width:200px">
							</div>
							<h4 class="text-center">{{ $item->name }}</h4>
						</div>
						<hr>
						<div class="card-body" style="height:250px">
							<p>Type: {{ $item->category->name }}</p>
							<p>Description: {{ $item->description }}</p>
							<p>Designer/s:<br>
								@foreach($item->creators as $creator)
								<ul>
									<li>{{ $creator->name }}</li>
								</ul>
								@endforeach
							</p>
							<p>Availability: <span class="{{ $item->status_id == 1 ? "text-success":"text-danger" }}">{{ $item->status->name }}</span></p>
						</div>

						<div class="card-footer">
							<a href="/edit-dress/{{ $item->id }}" class="btn btn-info btn-block my-1">Edit</a>
							@if(Auth::user()->role_id == 1)
								<form action="/delete-dress/{{ $item->id }}" method="POST">
									@csrf
									@method('DELETE')
									@if(!isset($item->deleted_at))
										<button class="btn btn-danger btn-block form-control my-1">Delete</button>
									@else
										<a href="/return-dress/{{ $item->id }}" class="btn btn-block btn-success">Make Available</a>
									@endif
								</form>
							@endif
						</div>
					</div>
				</div>
			@endforeach
		</div>
	</div>
@endsection