@extends('layouts.template')
@section('content')
	<h1 class="text-center py-5">Designers</h1>
	<div class="col-lg-12">
		
		<div class="row" style="margin-left: 100px">
			@foreach($creators as $creator)
			<div class="col-lg-4 my-5">
				<div class="card">
					<div class="card-head d-flex align-items-center justify-content-center">
						<h4 style="margin-top:30px">{{ $creator->name }}</h4>
					</div>
					<hr>
						<div class="card-body d-flex flex-column justify-content-center" style="height:250px">
							<p>{{ $creator->contact_number }}</p>
							<p><a href="http://{{ $creator->email }}" target="_blank">{{ $creator->email }}</a></p>
							<p>{{ $creator->address }}</p>
							<p><a href="http://{{ $creator->website }}">{{ $creator->website }}</a></p>
						</div>
						@if(Auth::user()->role_id == 1)
							<div class="card-footer">
								<a href="/edit-designer/{{ $creator->id }}" class="btn btn-block btn-info my-1">Edit Designer</a>
								<form class="form-group" action="/delete-creator/{{ $creator->id }}" method="POST">
									@csrf
									@method('DELETE')
									<button class="btn btn-block btn-danger my-1">Delete</button>
								</form>
							</div>
						@endif
				</div>
			</div>
			@endforeach

		</div>
	</div>
@endsection