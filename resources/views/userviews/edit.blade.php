@extends('layouts.app')
@section('content')
	
	<h1 class="text-center py-5">Edit Dress</h1>

	<div class="container">
		<div class="row">
			<div class="col-lg-6 offset-lg-3">
				<form action="/edit-dress/{{ $item->id }}" method="POST">
					@csrf
					@method('PATCH')

					<div class="form-group">
						<label for="name">Name:</label>
						<input type="text" name="name" class="form-control" value="{{ $item->name }}" readonly>
					</div>

					<div class="form-group">
						<label for="description">Description:</label>
						<input type="text" name="description" class="form-control" value="{{ $item->description }}">
					</div>
					
					<div class="form-group">
						<label for="creator_id">Designer/s:</label>
						@foreach($item->creators as $creators)
							<input type="hidden" name="creator_id" class="form-control" value="{{ $creators->id }}">
							<input type="text" name="creator_name" class="form-control" placeholder="{{ $creators->name }}" disabled>
						@endforeach

					</div>

					<div class="form-group">
						<label for="category_id">Category:</label>
						<input type="hidden" name="category_id" value="{{ $item->category_id }}">
						<input type="text" name="category_name" placeholder="{{ $item->category->name }}" class="form-control" disabled>
					</div>

					<div class="form-group">
						<label for="status_id">Dress Status:</label>
						<select name="status_id" class="form-control">
							@foreach($statuses as $status)
								<option value="{{ $status->id }}" {{ $item->status_id == $status->id ? "selected" : ""}}>{{ $status->name }}</option>
							@endforeach
						</select>
					</div>
					<button type="submit" class="btn btn-info">Submit</button>
				</form>
			</div>
		</div>
	</div>

@endsection