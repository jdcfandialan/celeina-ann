@extends('layouts.app')
@section('content')
	<h1 class="text-center py-5">Edit Profile</h1>
	<div class="row">
		<div class="col-lg-6 offset-lg-3">
			<form action="/edit-profile/{{ $user->id }}" method="POST" enctype="multipart/form-data">
					@csrf
					@method('PATCH')

					<div class="form-group">
						<label for="firstName">First Name:</label>
						<input type="text" name="firstName" class="form-control" value="{{ $user->firstName }}" readonly>
					</div>
					<div class="form-group">
						<label for="lastName">Last Name:</label>
						<input type="text" name="lastName" class="form-control" value="{{ $user->lastName }}" readonly>
					</div>
					<div class="form-group">
						<label for="contactNumber">Contact Number</label>
						<input type="text" name="contactNumber" class="form-control" value="{{ $user->contactNumber }}">
					</div>
					<div class="form-group">
						<label for="email">Email:</label>
						<input type="text" name="email" class="form-control" value="{{ $user->email }}" readonly>
					</div>
					<div class="form-group">
						<label for="image">Profile Picture:</label>
						<input type="file" name="image" class="form-control">
					</div>
					<input type="hidden" name="previousURL" value="{{ $previous }}">
					<button type="submit" class="btn btn-info">Submit</button>
				</form>
		</div>
	</div>
@endsection