@extends('layouts.template')
@section('content')

	<div class="row" style="margin-left: 200px">
		<div class="col-lg-9 offset-lg-1">
			<h4 class="py-5 text-center">Employee Search</h4>
			<div class="row">
				@foreach($userSearch as $user)
					<div class="col-lg-4 my-5">
						<div class="card">
							<div class="card-head d-flex justify-content-center align-items-center">
								<img src="{{ asset($user->imgPath) }}" alt="{{ $user->imgPath }}" style="object-fit: contain;height:100px;width:100px" class="img-thumbnail rounded-circle">
							</div>
							<div class="card-body" style="height:280px">
								<p>Name: {{ $user->firstName }} {{ $user->lastName}}</p>
								<p>Email: {{ $user->email }}</p>
								<p>Contact Number: {{ $user->contactNumber }}</p>
							</div>
							<div class="card-footer">
								<a href="/admin-edit/{{ $user->id }}" class="btn btn-block btn-info">Edit</a>
								<form action="/deactivate-employee/{{ $user->id }}" method="POST">
									@csrf
									@method('DELETE')
									@if($user->deleted_at == null)
										<button class="btn btn-danger btn-block my-1">Deactivate</button>
									@else
										<a href="/reactivate-employee/{{ $user->id }}" class="btn btn-success btn-block my-1">Reactivate</a>
									@endif
								</form>
							</div>
						</div>
					</div>
				@endforeach
			</div>
			<hr>

			<h4 class="py-5 text-center">Designer Search</h4>
			<div class="row">
				@foreach($designerSearch as $creator)
					<div class="col-lg-5 my-5">
						<div class="card">
							<div class="card-head d-flex align-items-center justify-content-center">
								<h4 style="margin-top:30px">{{ $creator->name }}</h4>
							</div>
							<hr>
							<div class="card-body d-flex flex-column justify-content-center" style="height:250px">
								<p>{{ $creator->contact_number }}</p>
								<p><a href="http://{{ $creator->email }}" target="_blank">{{ $creator->email }}</a></p>
								<p>{{ $creator->address }}</p>
								<p><a href="http://{{ $creator->website }}">{{ $creator->website }}</a></p>
							</div>
							@if(Auth::user()->role_id == 1)
								<div class="card-footer">
									<a href="/edit-designer/{{ $creator->id }}" class="btn btn-block btn-info my-1">Edit Designer</a>
									<form class="form-group" action="/delete-creator/{{ $creator->id }}" method="POST">
										@csrf
										@method('DELETE')
										<button class="btn btn-block btn-danger my-1">Delete</button>
									</form>
								</div>
							@endif
						</div>
					</div>
				@endforeach
			</div>
			<hr>
			<h4 class="py-5 text-center">Listing Search</h4>
			<div class="row">
				@foreach($itemSearch as $item)
					<div class="col-lg-4 my-5">
						<div class="card">
							<div class="card-head">
								<div class="d-flex justify-content-center align-items-center">
									<img src="{{ $item->imgPath }}" alt="broken img" style="object-fit: contain;height: 200px;width:200px">
								</div>
								<h4 class="text-center">{{ $item->name }}</h4>
							</div>
							<hr>
							<div class="card-body">
								<p>Type: {{ $item->category->name }}</p>
								<p>Description: {{ $item->description }}</p>
								<p>Designer/s:
									@foreach($item->creators as $creator)
									<ul>
										<li>{{ $creator->name }}</li>
									</ul>
									@endforeach
								</p>
								<p>Availability: <span class="{{ $item->status_id == 1 ? "text-success":"text-danger" }}">{{ $item->status->name }}</span></p>
							</div>

							<div class="card-footer">
								<a href="/edit-dress/{{ $item->id }}" class="btn btn-info btn-block my-1">Edit</a>
								@if(Auth::user()->role_id == 1)
									<form action="/delete-dress/{{ $item->id }}" method="POST">
										@csrf
										@method('DELETE')
										@if(!isset($item->deleted_at))
											<button class="btn btn-danger btn-block form-control my-1">Delete</button>
										@else
											<a href="/return-dress/{{ $item->id }}" class="btn btn-block btn-success">Make Available</a>
										@endif
									</form>
								@endif
							</div>
						</div>
					</div>
				@endforeach
			</div>
			<hr>
		</div>
	</div>
@endsection