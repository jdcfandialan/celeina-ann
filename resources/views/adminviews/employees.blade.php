@extends('layouts.template')
@section('content')
	
	<h1 class="text-center py-5">Employees</h1>
	<div class="col-lg-12">
		<div class="row" style="margin-left: 90px">
			@foreach($users as $user)
				@if(Auth::user()->id != $user->id)
				<div class="col-lg-4">
					<div class="card my-3">
						<div class="card-head d-flex justify-content-center align-items-center">
							<img src="{{ asset($user->imgPath) }}" alt="{{ $user->imgPath }}" style="object-fit: contain;height:100px;width:100px" class="img-thumbnail rounded-circle">
						</div>
						<div class="card-body" style="height:180px">
							<p>Name: {{ $user->firstName }} {{ $user->lastName}}</p>
							<p>Email: {{ $user->email }}</p>
							<p>Contact Number: {{ $user->contactNumber }}</p>
						</div>
						<div class="card-footer">
							<a href="/admin-edit/{{ $user->id }}" class="btn btn-block btn-info">Edit</a>
							<form action="/deactivate-employee/{{ $user->id }}" method="POST">
								@csrf
								@method('DELETE')
								@if($user->deleted_at == null)
									<button class="btn btn-danger btn-block my-1">Deactivate</button>
								@else
									<a href="/reactivate-employee/{{ $user->id }}" class="btn btn-success btn-block my-1">Reactivate</a>
								@endif
							</form>
						</div>
					</div>
				</div>
				@endif
			@endforeach
		</div>
	</div>

@endsection