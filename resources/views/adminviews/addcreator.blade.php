@extends('layouts.app')
@section('content')
	<h1 class="text-center py-5">Add Creator</h1>

	<div class="container">
		<div class="row">
			<div class="col-lg-6 offset-lg-3">
				<form action="/add-designer" method="POST">
					@csrf
					@method('POST')

					<div class="form-group">
						<label for="name">Designer Name:</label>
						<input type="text" name="name" class="form-control">
					</div>
					<div class="form-group">
						<label for="contact_number">Contact Number:</label>
						<input type="text" name="contact_number" class="form-control">
					</div>
					<div class="form-group">
						<label for="email">Email/Messenger:</label>
						<input type="text" name="email" class="form-control">
					</div>
					<div class="form-group">
						<label for="address">Business Address:</label>
						<input type="text" name="address" class="form-control">
					</div>
					<div class="form-group">
						<label for="website">Website Link:</label>
						<input type="text" name="website" class="form-control">
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-info">Add Designer</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection