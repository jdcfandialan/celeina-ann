@extends('layouts.app')
@section('content')
	
	<h1 class="text-center py-5">Add Dress</h1>
	<div class="row">
		<div class="col-lg-6 offset-lg-3">
			<form action="/add-dress" method="POST" enctype="multipart/form-data">
				@csrf

				<div class="form-group">
					<label for="name">Dress name:</label>
					<input type="text" name="name" class="form-control">
				</div>
				<div class="form-group">
					<label for="description">Description:</label>
					<input type="text" name="description" class="form-control">
				</div>
				<div class="form-group">
					<label>Designer/s:</label>
					<br>
					@foreach($creators as $creator)
						<input type="checkbox" name="creator_{{ $creator->id }}" value="{{ $creator->id }}">
						<label for="creator_{{ $creator->id }}">{{ $creator->name }}</label>
						<br>
					@endforeach
				</div>
				<div class="form-group">
					<label for="category_id">Category:</label>
					<select name="category_id" class="form-control">
						<option selected disabled>Select a category</option>
						@foreach($categories as $category)
							<option value="{{ $category->id }}">{{ $category->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="image">Dress Image:</label>
					<input type="file" name="image" class="form-control">
				</div>
				<button tpye="submit" class="btn btn-info">Add Dress</button>
			</form>
		</div>
	</div>

@endsection