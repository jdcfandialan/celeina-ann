<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \App\Item;
use \App\Category;
use Illuminate\Database\Eloquent\SoftDeletes;

class Creator extends Model
{
    public function items(){
    	return $this->belongsToMany('\App\Item')->withTimeStamps();
    }

    public function categories(){
    	return $this->belongsToMany('\App\Category')->withTimeStamps();
    }

    use softDeletes;
}
