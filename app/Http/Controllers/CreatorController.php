<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Creator;

class CreatorController extends Controller
{
    public function index(){
    	$creators = Creator::all();

    	return view('creators', compact('creators'));
    }
    
    public function add(){
    	return view('adminviews.addcreator');
    }

    public function addDesigner(Request $req){
    	$rules = array(
    		"name" => "required",
    		"contact_number" => "required",
    		"email" => "required",
    		"address" => "required",
    		"website" => "required"
    	);

    	$this->validate($req, $rules);

    	$newDesigner = new Creator;

    	$newDesigner->name = $req->name;
    	$newDesigner->contact_number = $req->contact_number;
    	$newDesigner->email = $req->email;
    	$newDesigner->address = $req->address;
    	$newDesigner->website = $req->website;

    	$newDesigner->save();

    	return redirect('/designers');
    }

    public function edit($id){
    	$creator = Creator::find($id);

    	return view('adminviews.editcreator', compact('creator'));
    }

    public function update($id, Request $req){
    	$rules = array(
    		"name" => "required",
    		"contact_number" => "required",
    		"email" => "required",
    		"address" => "required",
    		"website" => "required"
    	);

    	$this->validate($req, $rules);

    	$editDesigner = Creator::find($id);

    	$editDesigner->name = $req->name;
    	$editDesigner->contact_number = $req->contact_number;
    	$editDesigner->email = $req->email;
    	$editDesigner->address = $req->address;
    	$editDesigner->website = $req->website;

    	$editDesigner->save();

    	return redirect('/designers');
    }

    public function destroy($id){
    	$designer = Creator::find($id);

    	$designer->delete();

    	return redirect('/designers');
    }
}
