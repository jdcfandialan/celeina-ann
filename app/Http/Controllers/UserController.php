<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use \App\User;

class UserController extends Controller
{
    public function index(){
    	$users = User::withTrashed()->get();

    	return view('/adminviews.employees', compact('users'));
    }

    public function delete($id){
    	$user = User::find($id);

    	$user->delete();

    	return redirect('/employees');
    }

    public function reactivate($id){
    	$user = User::withTrashed()->find($id)->restore();

    	return redirect('/employees');
    }

    public function edit($id){
        $user = User::withTrashed()->find($id);
        $previous = URL::previous();

        return view('editprofile', compact('user','previous'));
    }

    public function adminEdit($id){
        $user = User::withTrashed()->find($id);
        $previous = URL::previous();

        return view('adminviews.editprofile', compact('user','previous'));
    }

    public function update($id, Request $req){
        $rules = array(
            "firstName" => "required",
            "lastName" => "required",
            "contactNumber" => "required",
            "email" => "required",
            "image" => "image|mimes:jpeg, jpg, png, gif, tiff, tif, webp"
        );

        $this->validate($req, $rules);

        $editUser = User::find($id);

        $editUser->firstName = $req->firstName;
        $editUser->lastName = $req->lastName;
        $editUser->email = $req->email;
        $editUser->contactNumber = $req->contactNumber;

        $image = $req->file('image');
        $image_name = time().".".$image->getClientOriginalExtension();

        $destination = "images/";

        $image->move($destination, $image_name);

        $editUser->imgPath = $destination.$image_name;

        $editUser->save();

        return redirect($req->previousURL);
    }
}
