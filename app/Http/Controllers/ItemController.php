<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Item;
use \App\Category;
use \App\Status;
Use \App\Creator;
use Auth;

class ItemController extends Controller
{
    public function index(){
    	$items = Item::withTrashed()->get();

    	return view('listing', compact('items'));
    }

    public function edit($id){
    	$item = Item::find($id);

    	$categories = Category::all();

    	$statuses = Status::all();

    	return view('userviews.edit', compact('item','categories','statuses'));
    }

    public function update($id, Request $req){
    	$rules = array(
    		"name" => "required",
    		"description" => "required",
    		"category_id" => "required",
    		"status_id" => "required"
    	);

    	$this->validate($req, $rules);

    	$editItem = Item::find($id);

    	$editItem->name = $req->name;
    	$editItem->description = $req->description;
    	$editItem->status_id = $req->status_id;
    	$editItem->category_id = $req->category_id;

    	$editItem->save();

    	return redirect('/listings');
    }

    public function add(){
        $categories = Category::all();
        $statuses = Status::all();
        $creators = Creator::all();

        return view('adminviews.adddress', compact('categories', 'statuses', 'creators'));
    }

    public function addDress(Request $req){
       
        $rules = array(
            "name" => "required",
            "description" => "required",
            "category_id" => "required",
            "image" => "required|image|mimes:jpeg, jpg, png, gif, tiff, tif, webp"
        );

        $this->validate($req, $rules);
        //form capture
        $newDress = new Item;


        $newDress->name = $req->name;
        $newDress->description = $req->description;
        $newDress->category_id = $req->category_id;
        $newDress->status_id = 1;

        //image handling
        $image = $req->file('image');
        $image_name = time().".".$image->getClientOriginalExtension();

        $destination = "images/";

        $image->move($destination, $image_name);

        $newDress->imgPath = $destination.$image_name;

        //save the new item to the database
        $newDress->save();

        $creators = Creator::all();

        foreach($creators as $creator){
            $test="creator_".$creator->id;
            if(isset($req->$test)){
                $newDress->creators()->attach($creator->id);
            }
        }
        
        return redirect('/listings');
    }

    public function delete($id){
        $item = Item::find($id);

        $item->status_id = 2;
        $item->save();

        $item->delete();

        return redirect('/listings');
    }

    public function makeAvailable($id){
        $item = Item::withTrashed()->find($id);

        $item->save();
        $item->status_id = 1;

        $item->restore();

        return redirect('/listings');
    }
}
