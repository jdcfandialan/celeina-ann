<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use \App\User;
use \App\Item;
use \App\Creator;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = User::find(Auth::user()->id);

        return view('home', compact('user'));
    }

    public function search(Request $req){

        $itemSearch = Item::withTrashed()->where('name', 'LIKE', '%'.$req->search.'%')->orWhere('description','LIKE', '%'.$req->search.'%')->get();
        $userSearch = User::withTrashed()->where('firstName', 'LIKE', '%'.$req->search.'%')->orWhere('lastName','LIKE', '%'.$req->search.'%')->get();
        $designerSearch = Creator::withTrashed()->where('name', 'LIKE', '%'.$req->search.'%')->get();

        return view('search', compact('itemSearch', 'userSearch', 'designerSearch'));
    }
}
