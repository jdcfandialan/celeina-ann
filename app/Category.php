<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \App\Creator;

class Category extends Model
{
    public function creators(){
    	return $this->belongsToMany('\App\Creator')->withTimeStamps();
    }
}
