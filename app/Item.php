<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \App\Category;
use \App\Status;
use \App\Creator;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    public function category(){
    	return $this->belongsTo('\App\Category');
    }

    public function status(){
    	return $this->belongsTo('\App\Status');
    }

    public function creators(){
    	return $this->belongsToMany('\App\Creator')->withTimeStamps();
    }

    use softDeletes;
}